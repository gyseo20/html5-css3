# HTML5, CSS3 스터디

### [이론] 코코아 톡 암호화 코딩

- [01-01] title 태그, a 태그, _blank
- [01-02] `<!DOCTYPE html>`, `<head>`, `<body>` `<h1>`~`<h6>`
- [01-03] 메타태그 author, description
- [01-04] 시맨틱, 논시맨틱태그, header, article, section, div, span
- [01-05] id, class
- [02-01] css 구문
- [02-02] html 문서에서 css 사용하기
- [03-01] - [03-06] 박스 모델, 컨텐츠, 보더, 패딩, 마진
- [04-01] 가상 셀렉터 :first-child :last-child input[type="password"] :nth-child(2n+1) input[required="required"]
- [05-01] :hover :active :focus element states